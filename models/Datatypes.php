<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "datatypes".
 *
 * @property integer $id
 * @property integer $catid
 * @property string $type
 * @property string $resource
 */
class Datatypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'datatypes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['catid'], 'integer'],
            [['resource'], 'required'],
            [['resource'], 'string'],
            [['type'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'catid' => 'Catid',
            'type' => 'Type',
            'resource' => 'Resource',
        ];
    }
}
