<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "content_description".
 *
 * @property integer $id
 * @property string $externalpage
 * @property string $title
 * @property string $description
 * @property integer $priority
 * @property string $topic
 * @property string $main
 */
class ContentDescription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'content_description';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['externalpage', 'title', 'description', 'topic'], 'required'],
            [['externalpage', 'title', 'description', 'topic', 'main'], 'string'],
            [['priority'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'externalpage' => 'Externalpage',
            'title' => 'Title',
            'description' => 'Description',
            'priority' => 'Priority',
            'topic' => 'Topic',
            'main' => 'Main',
        ];
    }
}
