<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "content_links".
 *
 * @property integer $id
 * @property integer $catid
 * @property string $topic
 * @property string $type
 * @property string $resource
 */
class ContentLinks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'content_links';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['catid'], 'integer'],
            [['topic', 'resource'], 'required'],
            [['topic', 'resource'], 'string'],
            [['type'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'catid' => 'Catid',
            'topic' => 'Topic',
            'type' => 'Type',
            'resource' => 'Resource',
        ];
    }
}
