<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "structure".
 *
 * @property integer $id
 * @property integer $catid
 * @property string $name
 * @property string $title
 * @property string $description
 */
class Structure extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'structure';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['catid'], 'integer'],
            [['name', 'title', 'description'], 'required'],
            [['name', 'title', 'description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'catid' => 'Catid',
            'name' => 'Name',
            'title' => 'Title',
            'description' => 'Description',
        ];
    }
}
