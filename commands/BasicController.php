<?php
namespace app\commands;
DEFINE('ECHO_STATS', true);
DEFINE('ECHO_STATS_FREQUENCY', 50000);

use yii\console\Controller;
use Yii;

class BasicController extends Controller
{
    public function printToConsole($string, $addNewStringToEnd = true, $addNewStringToStart = false)
    {
        Yii::$app->basicManager->printToConsole($string, $addNewStringToEnd, $addNewStringToStart);
    }

    public function printDot()
    {
        Yii::$app->basicManager->printDot();
    }
}
