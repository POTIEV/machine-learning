<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tables`.
 */
class m160523_133739_create_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        // Create content_description table
        $this->createTable('content_description', [
            'id' => $this->primaryKey(),
            'centroid_id' => $this->integer()->defaultValue(null),
            'externalpage' => $this->text()->notNull(),
            'title' => $this->text()->notNull(),
            'description' => $this->text()->notNull(),
            'topic' => $this->text()->notNull(),
            'main' =>  $this->text(),
            'priority' => $this->smallInteger()->notNull()->defaultValue(0)
        ]);

        // Create content_links table
        $this->createTable('content_links', [
            'id' => $this->primaryKey(),
            'catid' => $this->integer()->notNull()->defaultValue(0),
            'topic' => $this->text()->notNull(),
            'type' => $this->string(20)->notNull(),
            'resource' => $this->text()->notNull()
        ]);

        // Create structure table
        $this->createTable('structure', [
            'id' => $this->primaryKey(),
            'catid' => $this->integer()->notNull()->defaultValue(0),
            'name' => $this->text()->notNull(),
            'title' => $this->text()->notNull(),
            'description' => $this->text()->notNull(),
        ]);

        // Create datatypes table
        $this->createTable('datatypes', [
            'id' => $this->primaryKey(),
            'catid' => $this->integer()->notNull()->defaultValue(0),
            'type' => $this->string(20)->notNull(),
            'resource' => $this->text()->notNull()
        ]);

        // Create dictionary
        $this->createTable('dictionary', [
            'id' => $this->primaryKey(),
            'val' => $this->string(40),
            'count' => $this->integer()->defaultValue(1)
        ]);

        $this->createIndex('idxVal', 'dictionary', 'val', true);

        // Create centriod
        $this->createTable('centroid', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'coords' => $this->text()->notNull(),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('content_description');
        $this->dropTable('content_links');
        $this->dropTable('structure');
        $this->dropTable('datatypes');
        $this->dropTable('dictionary');
    }
}
